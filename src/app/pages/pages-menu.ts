import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,

  },
  {
    title: 'Home',
    icon: 'nb-trash',
    children: [
      {
        title: 'Page',
        link: '/pages/page',
      },
  ]},
    ];
