import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Created with ♥ by <b><a target="_blank">Arfaoui Youssef</a></b> 2019</span>
    <div class="socials">

    </div>
  `,
})
export class FooterComponent {
}
