/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AuthGuard } from './auth/auth-guard.service';
import { AuthService } from './auth/auth-service.service';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { RequestPasswordComponent } from './auth/request-password/request-password.component';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';

export const firebaseConfig = {
  firebase: {
    apiKey: 'AIzaSyCRXcmqD0Au1SvlASGIfg0EO8gQlWnqOwA',
    authDomain: 'my-project-1519911432392.firebaseapp.com',
    databaseURL: 'https://my-project-1519911432392.firebaseio.com',
    projectId: 'my-project-1519911432392',
    storageBucket: 'my-project-1519911432392.appspot.com',
    messagingSenderId: '394335563269',


  },
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent, RegisterComponent, RequestPasswordComponent, ResetPasswordComponent ,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,

    AngularFireModule.initializeApp(firebaseConfig.firebase), AngularFirestoreModule, AngularFireAuthModule,

    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    AuthGuard, AuthService,
  ],
})
export class AppModule {
}
